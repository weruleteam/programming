#include "helloWorld.h"
#include <iostream>
using namespace System;
using namespace System::Windows::Forms;
using namespace std;

int main(array<String^>^ args) {

	//this statement uses the CLI library
	Console::WriteLine(L"Hello World! (through CLI)");

	//this statement uses the iostream librabry
	cout << "Hello World! (using iostream)" << endl;

	//this statement runs the form found in the project and shows it
	Application::Run(gcnew ramiLukata_Assignment6::helloWorld);
}