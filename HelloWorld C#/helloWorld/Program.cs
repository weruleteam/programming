﻿using System;

namespace helloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            // variable declaration and initialization
            string myString = "This is my first C# program!";
            string usingLinux = "This project was completed using Visual Code (Linux)";
            int first = 28;
            int second = 29;

            // outputting to the screen
            Console.WriteLine("Hello World! My name is Rami!");
            Console.WriteLine(myString);
            Console.WriteLine("This project is for Assignment 1 for IS 381");
            Console.WriteLine(("Math: 28 + 29 =", first+second) + "\r\n" + usingLinux);
        }
    }
}
